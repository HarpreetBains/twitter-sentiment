# Twitter Sentiment

## Prerequisites

To run this project [Docker CE](https://docs.docker.com/install/) will need to be installed on your machine.

## Installation

Clone this repo

```bash
git clone git@bitbucket.org:HarpreetBains/twitter-sentiment.git
```

## Setup

Build the backend image
```bash
docker build -t backend [PATH TO 'twitter-sentiment/docker/backend']
```
Build the frontend image
```bash
docker build -t frontend [PATH TO 'twitter-sentiment/docker/frontend']
```
Wait for both images to be built (this can take a while the first time). 

## Usage

After the images are built run both of them.

Backend:
```bash
docker run -it --rm -p 5000:5000 backend
```

Frontend:
```bash
docker run -it --rm -p 8080:8080 frontend
```

After both are running head over to [http://localhost:8080/](http://localhost:8080/)