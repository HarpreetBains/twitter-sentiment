import os
import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
import keras
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.preprocessing.sequence import pad_sequences
import pickle
import twitter_credentials
from flask import Flask, request, jsonify
from flask_cors import CORS
import atexit

import re
import tweepy

class TweepyWrapper:
    def __init__(self):
        self.setUpTwitterApi()
        
    def setUpTwitterApi(self):
        auth = tweepy.OAuthHandler(twitter_credentials.API_KEY, twitter_credentials.API_SECRET)
        auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_TOKEN_SECRET)
        self.api = tweepy.API(auth)
        self.max_tweets = 100
    
    def getTweets(self, query):
        query = query + ' -filter:media -filter:retweets -filter:links lang:en'
        full_tweets = [tweet.text for tweet in tweepy.Cursor(self.api.search, q=query).items(self.max_tweets)]
        tweets = []
        for tweet in full_tweets:
            tweet = tweet.replace('\n', ' ')
            tweet = tweet.replace('\r', ' ')
            tweet = re.sub(r'[,!@#$%^&*)(|/><";:.?\'\\}{]','',tweet)
            tweet = tweet.split(' ')[-15:]
            tweets.append(tweet)
        return (full_tweets, tweets)
    
class Network:
    def __init__(self, tokenizer):
        self.setUpModel()
        self.tokenizer = tokenizer
            
    def decode(self, outputs):
        decoded_outputs = []
        for output in outputs:
            index = output.argmax()
            if index == 0:
                decoded_outputs.append('negative')
            elif index == 1:
                decoded_outputs.append('neutral')
            elif index == 2:
                decoded_outputs.append('positive')
        return decoded_outputs
    
    def setUpModel(self):
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())
        keras.backend.set_session(sess)
        sess.run(tf.tables_initializer())
        atexit.register(sess.close)
        
        self.model = load_model('final_model_combo.h5')
        
    def clasifyTweets(self, tweets):
        x = pad_sequences(self.tokenizer.texts_to_sequences(tweets), maxlen=15, padding='pre', truncating='pre')
        result = self.model.predict(x)
        return self.decode(result)

app = Flask(__name__)
CORS(app)

with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

def clasifyTweets(query):
    keras.backend.clear_session()
    twitter = TweepyWrapper()
    network = Network(tokenizer)
    tweets = twitter.getTweets(str(query))
    results = network.clasifyTweets(tweets[1])
    return jsonify(tweets=tweets[0], classifications=results)  
    
@app.route('/api/classify', methods=['POST'])
def onRequest():
    query = request.get_json()['query']
    results = clasifyTweets(query)
    return results 

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
